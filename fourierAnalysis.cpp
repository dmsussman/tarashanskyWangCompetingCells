#include "std_include.h" // std library includes, definition of Dscalar, Dscalar2, etc.. has a "using namespace std" in it.
#include "Matrix.h"//for when I might need 2x2 matrix manipulations
#include "box.h"//plausible helpful for dealing with periodic boundary conditions in 2D
#include "functions.h" // where a lot of the work happens

//we'll use TCLAP as our command line parser 
#include <tclap/CmdLine.h>
using namespace TCLAP; 

/*!
command line parameters help identify a data directory and a filename... the output is a text file
(in the data/ directory rooted here) containing easy-to-read fourier transforms of the height-map
representation of the extremal interfaces for each point in time
*/
int main(int argc, char*argv[])
{

    // wrap tclap in a try block
    try
    {

	// cmd("command description message", delimiter, version string)
	CmdLine cmd("interface parsing and analyzing", ' ', "V0.0");
    //ValueArg<T> variableName("shortflag","longFlag","description",required or not, default value,description of the type",CmdLine object to add to
    ValueArg<int> programSwitchArg("z","programSwitch","an integer controlling program branch",false,0,"int",cmd);
    ValueArg<int> replicateArg("r","replicate","integer for sim idx",false,0,"int",cmd);
    ValueArg<int> boxLengthArg("b","boxLength","integer for system size",false,10,"int",cmd);
    ValueArg<int> lineLengthArg("l","CSVlineLength","number of csv columns", false, 2001,"int",cmd);
    ValueArg<string> mutationRateArg("m","mutationRate","string for mutation rate", false, "0.00001","string",cmd);
    ValueArg<string> selectionArg("s","selection","string for selection", false, "0.05","string",cmd);
	ValueArg<string> dirnameArg("d","directoryName","Directory with data",false,"/Users/dmsussma/data/BoWang/cellInterface/pbc1/","string",cmd);
	ValueArg<string> cellArg("c","cellType","cell type -- ONE or TWO",false,"ONE","string",cmd);

    //parse the arguments
    cmd.parse( argc, argv );

    int boxLength = boxLengthArg.getValue();
    int replicate = replicateArg.getValue();
    int programSwitch = programSwitchArg.getValue();
    string dirName = dirnameArg.getValue();
    int lineLength = lineLengthArg.getValue();
    string mutationRate = mutationRateArg.getValue();
    string selection = selectionArg.getValue();
    string cell = cellArg.getValue();

    char dirname[256];
    char filename[256];
    sprintf(dirname,"%s",dirName.c_str());
    sprintf(filename,"surface_tension_length_%i_replicate_%i_%s_%s_%s",boxLength,replicate,mutationRate.c_str(),selection.c_str(),cell.c_str());

    cout << boxLength << "\t" << replicate << "\t" <<programSwitch << endl;
    cout << "reading directory: " << dirname<< endl;
    printf ("Reading in data file %s\n",filename);

    /*
    if command line option z = 0 (the default setting), extract the interface height map from the
    selected file, fourier transform it, and save results in a text file in the data directory
    */
    if(programSwitch == 0)
        {
        //for each time point, get the lower interface of cell TWO and the upper interface of cell ONE
        vector< vector< Dscalar2> > lowerCellInterface;
        vector< vector< Dscalar2> > upperCellInterface;
        reduceCSVtoHeightMaps(dirname,filename,lineLength,upperCellInterface,lowerCellInterface);
        //each of the data structures is a vector of vector of Dscalar2's, where the inner vector is a sorted (by x-component) list of the most extremal cell at a given y-value

        //prepare to save data
        char outname[256];
        sprintf(outname,"./data/fourierTransformProfile_L%i_replicate%i_%s_%s_%s.txt",boxLength,replicate,mutationRate.c_str(),selection.c_str(),cell.c_str());
        ofstream outfile;
        outfile.open(outname);

        //cell ONE has the interesting interface at the top, cell TWO has the interesting interface at the bottom
        string testONE = "ONE";
        vector<Dscalar> qValues;
        vector<Dscalar2> realImComponents;
        vector<Dscalar2> currentInterface;
        for (int time = 0; time < upperCellInterface.size(); ++time)
            {
            //cout << "FT at time " << time << endl;
            if(cell == testONE)
                currentInterface = upperCellInterface[time];
            else
                currentInterface = lowerCellInterface[time];

            subtractMeanFromHeightMap(currentInterface);
            simpleFourier(currentInterface,qValues,realImComponents);

            //at each time, save a single row as a triple (q_1, Re(h(q))_1, Im(h(q))_1, q_2, Re(h(q))_2, Im(h(q))_2,...
            for (int qq = 0; qq < qValues.size(); ++qq)
                {
                outfile <<qValues[qq]<<"\t"<<realImComponents[qq].x<<"\t"<<realImComponents[qq].y<<"\t";
                };
            outfile <<"\n";
            };
        };

//The end of the tclap try
	} catch (ArgException &e)  // catch any exceptions
	{ cerr << "error: " << e.error() << " for arg " << e.argId() << endl; }
    return 0;
};
