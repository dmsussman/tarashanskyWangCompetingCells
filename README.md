# Basic info

This repository houses simple cpp code to analyze 2D cell data sent by Alec and Bo. The input
are a bunch of csv files where the column indicates the cartesian component and time point
of the simulation, and the row indicates that column's value (x_0, y_999, etc.) for a new cell.
Thus, the number of relevant rows is different in each column.

This was quickly built from a scrap heap of code written for other purposes, so some of the functions
and inc files might not be used... I'm not sure. Main cpp files are currently getDensityProfile.cpp, fourierAnalysis.cpp, and
oldTestProcessInterface.cpp, all of which can be called with various options; we're using TCLAP, so, e.g.,
"./getDensityProfile.out --help"  should return at least some minimal information

# Running the density profile calculation

(1) Compiling should be as simple as typing "make" when sitting in the root directory of this repository

(2) call ./getDensityProfile.out with a bunch of command line options. For instance, I might type:

`./getDensityProfile.out -r 0 -b 120 -m 1.584893192461114e-05 -s 0.05 -c ONE -d /Users/dmsussma/data/BoWang/newCellData/`

where "-r" is the replicate, "-b" is the length of the box, "-m" is a string for the mutation rate, "-s" is a string for
the selection, "-c" is either ONE or TWO, and "-d" is the directory where the file can be found.

(3) Output will be saved in the "data/" directory of the repository *which is gitignored and will not be synced*

# Running fourier reduction

Same as above, and using the same command line options, but with `./fourierAnalysis.out` as the executable

## gitignored stuff

By default, executables and compilation objects are not saved in the repository.
Nothing in the data directory should be included in the directory, either.


## poorly documented stuff
the inc/functions.h file contains a bunch of utility functions to process these sorts of files. Of note:

processCSV(......) takes a file of the given name and, under the guarantee
that one needs to read (linelength) number of columns, reads in the csv entries.

reduceCSVtoCellRows(......) takes a file and saves two data files, corresponding positions of the largest y-component and
smallest y-component cell at each value of x-component in a data file, at each time. That is, one gets
two height maps corresponding to an estimate of the upper and lower interfaces of the cells (assuming the
cells are in a horizontally oriented colony of strip geometry.

reduceCSVtoHeightMaps(......) does something similar, but saves the results to data structures rather than external files.

interfacialWidth(.....) takes two interfaces and returns information about their typical width

inc/extraMath.h contains a really dumb fourier integrator...this should be changed to either higher order integral or just a discrete sum ASAP
