#include "std_include.h" // std library includes, definition of Dscalar, Dscalar2, etc.. has a "using namespace std" in it.
#include "Matrix.h"//for when I might need 2x2 matrix manipulations
#include "box.h"//plausible helpful for dealing with periodic boundary conditions in 2D
#include "functions.h" // where a lot of the work happens

//we'll use TCLAP as our command line parser 
#include <tclap/CmdLine.h>
using namespace TCLAP; 

/*!
This cpp file was used prior to 4/16/18 for testing code and doing a few analyses of Alec's data
*/
int main(int argc, char*argv[])
{

    // wrap tclap in a try block
    try
    {

	// cmd("command description message", delimiter, version string)
	CmdLine cmd("interface parsing and analyzing", ' ', "V0.0");
    //ValueArg<T> variableName("shortflag","longFlag","description",required or not, default value,description of the type",CmdLine object to add to
    ValueArg<int> programSwitchArg("z","programSwitch","an integer controlling program branch",false,0,"int",cmd);
    ValueArg<int> replicateArg("r","replicate","integer for sim idx",false,0,"int",cmd);
    ValueArg<int> boxLengthArg("b","boxLength","integer for system size",false,10,"int",cmd);
    ValueArg<int> lineLengthArg("l","CSVlineLength","number of csv columns", false, 2001,"int",cmd);
	ValueArg<string> dirnameArg("d","directoryName","Directory with data",false,"/Users/dmsussma/data/BoWang/cellInterface/pbc1","string",cmd);

    //parse the arguments
    cmd.parse( argc, argv );

    int boxLength = boxLengthArg.getValue();
    int replicate = replicateArg.getValue();
    int programSwitch = programSwitchArg.getValue();
    string dirName = dirnameArg.getValue();
    int lineLength = lineLengthArg.getValue();

    char dirname[256];
    char filename[256];
    sprintf(dirname,"%s",dirName.c_str());
    cout << boxLength << "\t" << replicate << "\t" <<programSwitch << endl;
    cout << "reading directory: " << dirname<< endl;


    //if command line option z = -1, read the test files that Alec originally sent, then output two txt files in ./data/
    //corresponding to the upper- and lower-most cell in that file at each x coordinate for each time
    if(programSwitch == -1)
        {
        //Read in one of Alec's files...
        sprintf(filename,"1sf0");
        vector< vector< Dscalar> > filedata;
        printf ("Reading in data file %s\n",filename);
        reduceCSVtoCellRows(dirname,filename,lineLength,filedata);
        };

    //if command line option z = 0 (default), process one of the "surface_tension_length_X..." files, set by the "-b" command line options
    if(programSwitch == 0)
        {
        //Read in one of Alec's files...
        sprintf(filename,"surface_tension_length_%i_replicate_%i_ONE",boxLength,replicate);
        vector< vector< Dscalar> > filedata;
        printf ("Reading in data file %s\n",filename);
        reduceCSVtoCellRows(dirname,filename,lineLength,filedata);

        sprintf(filename,"surface_tension_length_%i_replicate_%i_TWO",boxLength,replicate);
        printf ("Reading in data file %s\n",filename);
        reduceCSVtoCellRows(dirname,filename,lineLength,filedata);
        };

    //if command line option z = 1, read both the upper and lower interfaces, calculated various widths as a function of time
    if(programSwitch == 1)
        {
        vector< vector< Dscalar2> > blankvec;
        //Read in one of Alec's files...

        //for each time point, get the lower interface of cell TWO and the upper interface of cell ONE
        vector< vector< Dscalar2> > lowerCellInterface;
        vector< vector< Dscalar2> > upperCellInterface;
        sprintf(filename,"surface_tension_length_%i_replicate_%i_ONE",boxLength,replicate);
        printf ("Reading in data file %s\n",filename);
        reduceCSVtoHeightMaps(dirname,filename,lineLength,upperCellInterface,blankvec);

        sprintf(filename,"surface_tension_length_%i_replicate_%i_TWO",boxLength,replicate);
        printf ("Reading in data file %s\n",filename);
        reduceCSVtoHeightMaps(dirname,filename,lineLength,blankvec, lowerCellInterface);
/*
        printf("vector sizes: %i\t %i\n",lowerCellInterface.size(),upperCellInterface.size());
        for (int jj = 0; jj < lowerCellInterface.size(); ++jj)
            printf("interface sizes: %i\t%i\n",lowerCellInterface[jj].size(),upperCellInterface[jj].size());
*/
        vector<Dscalar4> Ws(lowerCellInterface.size());
        for (int jj = 0; jj < lowerCellInterface.size(); ++jj)
            {
            interfacialWidth(lowerCellInterface[jj],upperCellInterface[jj],Ws[jj]);
            Ws[jj].x=(Dscalar) jj;
            //printf("interface: %f\t%f\t%f\t%f\n",Ws[jj].x,Ws[jj].y,Ws[jj].z,Ws[jj].w);
            }

        char outname[256];
        sprintf(outname,"./data/width_L%i_idx%i.txt",boxLength,replicate);
        ofstream outfile;
        outfile.open(outname);
        for (int jj = 0; jj < Ws.size(); ++jj)
            outfile <<Ws[jj].x<<"\t"<<Ws[jj].y<<"\t"<<Ws[jj].z<<"\t"<<Ws[jj].w<<"\n";

        outfile.close();
        };

    //if command line option z = 2, get density profile
    if(programSwitch == 2)
        {
        vector< vector< Dscalar> > density;
        //Read in one of Alec's files...

        sprintf(filename,"surface_tension_length_%i_replicate_%i_ONE",boxLength,replicate);
        printf ("Reading in data file %s\n",filename);
        reduceCSVtoDensityProfiles(dirname,filename,lineLength,1000,density);

        char outname[256];
        sprintf(outname,"./data/densityProfile_L%i_idx%i.txt",boxLength,replicate);
        ofstream outfile;
        outfile.open(outname);
        for (int yVal = 0; yVal < density[0].size(); ++yVal)
            {
            outfile << yVal << "\t";
            for (int time = 0; time < density.size(); ++time)
                outfile << density[time][yVal] <<"\t";
            outfile <<"\n";
            };

        outfile.close();
        };



//The end of the tclap try
	} catch (ArgException &e)  // catch any exceptions
	{ cerr << "error: " << e.error() << " for arg " << e.argId() << endl; }
    return 0;
};
