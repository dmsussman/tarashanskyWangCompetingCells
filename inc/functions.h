#ifndef CUFUNCTIONS_H
#define CUFUNCTIONS_H

#include "std_include.h"
#include "Matrix.h"
#include "extraMath.h"
#include "fileParsing.h"
#include <set>


//!accept a vector of Dscalar2's (representing the {x,h(x)}), and give as output a vector of {x,h(x)-<h>}
void subtractMeanFromHeightMap(vector<Dscalar2> &h)
    {
    Dscalar mean = 0.0;
    for (int ii = 0; ii < h.size();++ii)
        mean +=h[ii].y;
    mean = mean / h.size();
    for (int ii = 0; ii < h.size();++ii)
        h[ii].y -= mean;
    };

//!remove duplicate elements from a vector, preserving the order, using sets
template<typename T>
inline __attribute__((always_inline)) void removeDuplicateVectorElements(vector<T> &data)
    {
    set<T> currentElements;
    auto newEnd = std::remove_if(data.begin(),data.end(),[&currentElements](const T& value)
        {
        if (currentElements.find(value) != std::end(currentElements))
            return true;
        currentElements.insert(value);
        return false;
        });
    data.erase(newEnd,data.end());
    };

//What follows are a few functions that read in the CSV files and process them in different ways. This could be made more modular, but each is pretty short so I (DMS) don't care at the moment

/*
give one of Alec's files, get the average density profile (averaged for each x-bin)...these profiles
will be fit to tanh's to get a measure of the width
*/
void reduceCSVtoDensityProfiles(char *readDirName, char *filename, int lineLength,int maxHeight, vector<vector< Dscalar> > &density)
    {
    string rDirName(readDirName);
    string fName(filename);
    char readname[256];
    sprintf(readname,"%s%s.csv",rDirName.c_str(),fName.c_str());
    vector<vector<Dscalar> > filedata;
    processCSV(readname,lineLength,filedata);

    vector<Dscalar> baseDensity(maxHeight,0.);
    vector< vector<Dscalar> > ans(1000,baseDensity);

    for (int time = 0; time < 1000; ++time)
        {
        int idx = 0;
        vector<pair<Dscalar, Dscalar> > interfaces;
        pair <Dscalar,Dscalar> entry;
        Dscalar xval = filedata[idx][1+2*time];
        Dscalar yval = filedata[idx][2+2*time];
        while(xval > -1.0)
            {
            entry = make_pair(yval,xval);
            interfaces.push_back(entry);
            idx+=1;
            if(idx >=filedata.size()) break;
            xval = filedata[idx][1+2*time];
            yval = filedata[idx][2+2*time];
            }
        
        sort(interfaces.begin(),interfaces.end());
        //we now have all of the cells sorted by y-component!
        int Nc = interfaces.size();
        int ent = 0;
        while(ent < Nc)
            {
            Dscalar y = interfaces[ent].first;
            int e2 = ent;
            vector<Dscalar> xSet;
            while (e2 < interfaces.size() && interfaces[e2].first == y)
                {
                xSet.push_back(interfaces[e2].second);
                e2+=1;
                };
            Dscalar rho = (Dscalar)(xSet.size())/((Dscalar)Nc);

            ans[time][(int)y] = rho;
            ent=e2;
            };
        };
    density = ans;
    };


//read one of Alec's files, then save just the top most and bottom most cell at each value of x
void reduceCSVtoCellRows(char *readDirName, char *filename, int lineLength, vector<vector< Dscalar> > &filedata) 
    {
    string rDirName(readDirName);
    string fName(filename);
    char readname[256];
    sprintf(readname,"%s%s.csv",rDirName.c_str(),fName.c_str());

    char outname[256];
    sprintf(outname,"./data/Lower_%s.txt",fName.c_str());
    ofstream outfile1;
    outfile1.open(outname);
    sprintf(outname,"./data/Upper_%s.txt",fName.c_str());
    ofstream outfile2;
    outfile2.open(outname);

    processCSV(readname,lineLength,filedata);
    for (int time = 0; time < 1000; ++time)
        {
        int idx = 0;
        vector<pair<Dscalar, Dscalar> > interfaces;
        pair <Dscalar,Dscalar> entry;
        Dscalar xval = filedata[idx][1+2*time];
        Dscalar yval = filedata[idx][2+2*time];
        while(xval > -1.0)
            {
            entry = make_pair(xval,yval);
            interfaces.push_back(entry);
            idx+=1;
            if(idx >=filedata.size()) break;
            xval = filedata[idx][1+2*time];
            yval = filedata[idx][2+2*time];
            }

        sort(interfaces.begin(),interfaces.end());

        int ent = 0;
        while(ent < interfaces.size())
            {
            Dscalar x = interfaces[ent].first;
            int e2 = ent;
            vector<Dscalar> ySet;
            while (e2 < interfaces.size() && interfaces[e2].first == x)
                {
                ySet.push_back(interfaces[e2].second);
                e2+=1;
                };
            Dscalar yMax = *max_element(ySet.begin(),ySet.end());
            Dscalar yMin = *min_element(ySet.begin(),ySet.end());
            outfile1 << x <<"\t"<<yMin<<"\t";
            outfile2 << x <<"\t"<<yMax<<"\t";
            ent=e2;


            };
        outfile1<<"\n";
        outfile2<<"\n";
        };
    };

//Read one of Alec's files, then store the two height maps (of the top and bottom-most extremal cells) in the vectors. The height maps for each time point will be stored.
void reduceCSVtoHeightMaps(char *readDirName, char *filename, int lineLength, vector<vector< Dscalar2> > &upperInterface, vector<vector< Dscalar2> > &lowerInterface)
    {
    string rDirName(readDirName);
    string fName(filename);
    char readname[256];
    sprintf(readname,"%s%s.csv",rDirName.c_str(),fName.c_str());

    vector<vector< Dscalar> > filedata;
    processCSV(readname,lineLength,filedata);
    upperInterface.resize(1000);
    lowerInterface.resize(1000);

    for (int time = 0; time < 1000; ++time)
        {
        int idx = 0;
        vector<pair<Dscalar, Dscalar> > interfaces;
        pair <Dscalar,Dscalar> entry;
        Dscalar xval = filedata[idx][1+2*time];
        Dscalar yval = filedata[idx][2+2*time];
        while(xval > -1.0)
            {
            entry = make_pair(xval,yval);
            interfaces.push_back(entry);
            idx+=1;
            if(idx >=filedata.size()) break;
            xval = filedata[idx][1+2*time];
            yval = filedata[idx][2+2*time];
            }

        sort(interfaces.begin(),interfaces.end());

        int ent = 0;
        while(ent < interfaces.size())
            {
            Dscalar x = interfaces[ent].first;
            int e2 = ent;
            vector<Dscalar> ySet;
            while (e2 < interfaces.size() && interfaces[e2].first == x)
                {
                ySet.push_back(interfaces[e2].second);
                e2+=1;
                };
            Dscalar yMax = *max_element(ySet.begin(),ySet.end());
            Dscalar yMin = *min_element(ySet.begin(),ySet.end());
            Dscalar2 entry1 = make_Dscalar2(x,yMax);
            Dscalar2 entry2 = make_Dscalar2(x,yMin);
    
            if(x >0)
                {
                upperInterface[time].push_back(entry1);
                lowerInterface[time].push_back(entry2);
                };
            ent=e2;
            };
        };
    };

//take in two vectors of (Dscalar2) interface positions, output different measures of interfacial widths
void interfacialWidth(vector<Dscalar2> &interfaceA, vector<Dscalar2> &interfaceB, Dscalar4 &widths)
    {
    Dscalar meanA = vectorAverage(interfaceA).y;
    Dscalar meanB = vectorAverage(interfaceB).y;

    Dscalar widthSquaredA = 0.0;
    Dscalar widthSquaredB = 0.0;
    Dscalar widthDiffSquared = 0.0;
    
    for (int ii = 0; ii < interfaceA.size(); ++ii)
        {
        widthSquaredA += (interfaceA[ii].y - meanA)*(interfaceA[ii].y - meanA);
        widthSquaredB += (interfaceB[ii].y - meanB)*(interfaceB[ii].y - meanB);
        widthDiffSquared += (interfaceA[ii].y - interfaceB[ii].y)*(interfaceA[ii].y - interfaceB[ii].y);
        };

    widths.y = widthSquaredA / interfaceA.size();
    widths.z = widthSquaredB / interfaceA.size();
    widths.w = widthDiffSquared / interfaceA.size();
    };

#endif
