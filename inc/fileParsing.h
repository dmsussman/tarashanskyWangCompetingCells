#ifndef fileParse_H
#define fileParse_H

#include "std_include.h"

Dscalar stringOrNegative(string &reader)
    {
    if(reader.empty())
        return -1000;
    else
        return stod(reader);
    };

//read in a csv file formatted a la Alec. After calling this on a csv file of lineLength columns the data will be stored in the vec<vec<>> data structure
void processCSV(char *filename, int lineLength, vector<vector< Dscalar> > &filedata,bool verbose = true)
    {
    if(verbose)
        cout << "beginning processing of " << filename << endl;
    ifstream fileLength(filename);

    //figure how how many lines to read in, and set vector sizes appropriately
    string line;
    int len =0;
    while (getline(fileLength,line))
        len +=1;
    vector<Dscalar> linedata(lineLength,0.0);
    filedata = vector<vector<Dscalar> >(len,linedata);
    if(verbose)
        cout << "need to process " << len << " lines" << endl;
    fileLength.close();

    ifstream inFile(filename);
    //first, read the header of ,x0,y0,.....x999,y999
    getline(inFile,line);

    //now, read until the end of file. The first entry is unnecessary
    string reader;
    int counter = 0;
    while (getline(inFile,reader,','))
        {
        linedata[0] = stringOrNegative(reader);
        for (int ii = 1; ii < lineLength-1; ++ii)
            {
            getline(inFile,reader,',');
            linedata[ii] = stringOrNegative(reader);
            }
        getline(inFile,reader);
        linedata[lineLength-1] = stringOrNegative(reader);
        filedata[counter]=linedata;
        counter+=1;
        }
    inFile.close();
    };

//some files might be more easily read with this simple function... but probably not any of the ones we care about at the moment. read a data file of floats, where each line of the file has "lineLength" entries
void readFile(char *filename, int lineLength,vector<vector< Dscalar> > &filedata)
    {
    ifstream inFile;
    inFile.open(filename);

    vector<Dscalar> linedata(lineLength);
    int line = 0;
    Dscalar x;
    while (inFile >> x)
        {
        if(line < lineLength)
            {
            linedata[line] = x;
            line+=1;
            }
        else
            {
            filedata.push_back(linedata);
            line = 0;
            linedata[line] = x;
            line+=1;
            };
        };
    //the above doesn't append the final line, so...
    filedata.push_back(linedata);
    inFile.close();
    //printf("read in %i lines of data\n",filedata.size());
    };

#endif
