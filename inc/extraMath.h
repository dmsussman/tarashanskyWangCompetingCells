#ifndef extraMath_H
#define extraMath_H

#include "std_include.h"
#include "Matrix.h"

//!Calculate the determinant of a 2x2 matrix
HOSTDEVICE Dscalar Det2x2(const Dscalar &x11,const Dscalar &x12, const Dscalar &x21, const Dscalar &x22)
    {
    return x11*x22-x12*x21;
    };

//!The dot product between two vectors of length two.
HOSTDEVICE Dscalar dot(const Dscalar2 &p1, const Dscalar2 &p2)
    {
    return p1.x*p2.x+p1.y*p2.y;
    };

//!The norm of a 2-component vector
HOSTDEVICE Dscalar norm(const Dscalar2 &p)
    {
    return sqrt(p.x*p.x+p.y*p.y);
    };

//return the average of the components of a vector of Dscalars
Dscalar vectorAverage(vector<Dscalar> &vec)
    {
    Dscalar m = 0.0;
    int size = vec.size();
    for (int nn = 0; nn < size; ++nn)
        m += vec[nn];
    m /= size;
    return m;
    };
//return the average of the components of a vector of Dscalars
Dscalar2 vectorAverage(vector<Dscalar2> &vec)
    {
    Dscalar2 m = make_Dscalar2(0.0,0.0);
    int size = vec.size();
    for (int nn = 0; nn < size; ++nn)
        m = m + vec[nn];
    m.x /=size;
    m.y/=size;
    return m;
    };

/*
Take a simple estimate of the fourier transform of a function on some domain.
Input:  a vector<Dscalar2> of (x,y) pairs sorted by x-value.
Output: a vector<Dscalar> of q values evaluated
        a vector<Dscalar2> of the real and imaginary components of the FT at those q values
The calculation just trapezoid rules the integral in between each point. Very hacky
*/
void simpleFourier(vector<Dscalar2> &input, vector<Dscalar> &qValues, vector<Dscalar2> &realImaginaryComponents, Dscalar scaledQMax = 0.5)
    {
    int size = input.size();
    Dscalar xMin = input[0].x;
    Dscalar xMax = input[size-1].x;
    Dscalar Lx = xMax-xMin; 
    if (Lx <= 0)
        {
        cout << "improperly formatted data in FT routine" << endl;
        return;
        }
    int qSize = floor(scaledQMax*Lx);
    qValues.resize(qSize);
    realImaginaryComponents.resize(qSize);
    for (int qq = 0; qq < qSize; ++qq)
        {
        qValues[qq] = 2.0*PI*((Dscalar) (qq+1)) / Lx;
        //cout << qValues[qq] <<"\t";
        };
    //cout << endl;

    //Now, for each q-value, calculate the real and imaginary parts of the integral
    for (int qq = 0; qq < qSize; ++qq)
        {
        Dscalar negativeQ = -1.0*qValues[qq];
        Dscalar realPart = 0.0;
        Dscalar imPart = 0.0;
        
        Dscalar lastRealF = input[0].y*cos(negativeQ*input[0].x);
        Dscalar lastImF = input[0].y*sin(negativeQ*input[0].x);
        for (int xx = 1; xx < size; ++xx)
            {
            Dscalar nextRealF = input[xx].y*cos(negativeQ*input[xx].x);
            Dscalar nextImF = input[xx].y*sin(negativeQ*input[xx].x);

            realPart += (input[xx].x-input[xx-1].x)*0.5*(lastRealF+nextRealF);
            imPart += (input[xx].x-input[xx-1].x)*0.5*(lastImF+nextImF);

            lastRealF = nextRealF;
            lastImF = nextImF;
            };
        realImaginaryComponents[qq].x = realPart / Lx;
        realImaginaryComponents[qq].y = imPart / Lx;
        };
    };

#endif
